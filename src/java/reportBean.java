
import java.io.Serializable;
import java.math.BigDecimal;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@SessionScoped
@Named("reportBean")
public class reportBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Report[] reportList = new Report[]{
        new Report("AAPT", "N45234", new BigDecimal("79.08"), 2200, 8939, 2, 1),
        new Report("Optus", "N3454567", new BigDecimal("96.06"), 800, 5602, 3, 1),
        new Report("Iseek", "N234234", new BigDecimal("80.16"), 1610, 1196, 1, 2),
        new Report("AAPT", "N4554", new BigDecimal("73.00"), 6200, 8939, 2, 3),
        new Report("Optus", "N3454522", new BigDecimal("68.06"), 2800, 5602, 4, 1),};

    private static final Report[] reportListSecond = new Report[]{
        new Report("People Telecom", "N2222", new BigDecimal("89.33"), 4500, 7739, 2, 1),
        new Report("Amcor", "N789", new BigDecimal("66.06"), 800, 5602, 3, 1)
    };

    public Report[] getReportList() {
        return reportList;
    }

    public Report[] getReportListSecond() {
        return reportListSecond;
    }

    public static class Report {

        String isp;
        String fnn;
        BigDecimal bandwidth;
        Integer networksid;
        Integer kbps;
        Integer timer;
        Integer lastmonth;

        public Report(String isp, String fnn, BigDecimal bandwidth, Integer networksid, Integer kbps, Integer timer, Integer lastmonth) {
            this.isp = isp;
            this.fnn = fnn;
            this.bandwidth = bandwidth;
            this.networksid = networksid;
            this.kbps = kbps;
            this.timer = timer;
            this.lastmonth = lastmonth;
        }

        public String getIsp() {
            return isp;
        }

        public void setIsp(String isp) {
            this.isp = isp;
        }

        public String getFnn() {
            return fnn;
        }

        public void setFnn(String fnn) {
            this.fnn = fnn;
        }

        public BigDecimal getBandwidth() {
            return bandwidth;
        }

        public void setBandwidth(BigDecimal bandwidth) {
            this.bandwidth = bandwidth;
        }

        public Integer getNetworksid() {
            return networksid;
        }

        public void setNetworksid(Integer networksid) {
            this.networksid = networksid;
        }

        public Integer getKbps() {
            return kbps;
        }

        public void setKbps(Integer kbps) {
            this.kbps = kbps;
        }

        public Integer getTimer() {
            return timer;
        }

        public void setTimer(Integer timer) {
            this.timer = timer;
        }

        public Integer getLastmonth() {
            return lastmonth;
        }

        public void setLastmonth(Integer lastmonth) {
            this.lastmonth = lastmonth;
        }
    }
}
