function checkRow(selectedRow) {
    selectedRow.parent().find('.rf-dt-r').removeClass('green-row red-row');
    var checkValue = selectedRow.find('.check-value').text();
    checkValue = parseFloat(checkValue);
    console.log(checkValue);
    if (checkValue > 85) {
        selectedRow.addClass('red-row');
    }
    else if((checkValue > 70) && (checkValue < 84)){
        selectedRow.addClass('green-row');
    }
}